#pragma once

#include <memory>
#include <utility>
#include "SDLScreenManager.h"
class Application
{
public:
	Application();
	~Application();

public:
	bool run();

private:
	bool m_running;
	SDL_Event m_SDLEvent;
	std::shared_ptr<SDLScreenManager> m_screenManager;
};

