#pragma once



class Screen
{
public:
	/* Class Constructor & Destructor */
	virtual ~Screen() {}

public:
	/* General Public Methods */
	virtual void init() = 0;
	virtual void update() = 0;
	virtual void render() = 0;

private:
};

