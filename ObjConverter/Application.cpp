#include "Application.h"

Application::Application()
{
	m_running = true;

	/* Create, initialise and register ScreenManager with UtiltiesManager */
	m_screenManager = std::shared_ptr<SDLScreenManager>(new SDLScreenManager());
	m_screenManager->createWindow("Object Conversion Project", 1920, 1080);
	m_screenManager->init();


}


Application::~Application()
{

}


bool Application::run()
{
	
	while (m_running == true)
	{

		while (SDL_PollEvent(&m_SDLEvent))
		{
			switch (m_SDLEvent.type)
			{
			case SDL_WINDOWEVENT:
				switch (m_SDLEvent.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE:
					m_running = false;
					break;
				}
				break;
			case SDL_QUIT:
				m_running = false;
				break;
			}

		}

		m_screenManager->update();
		m_screenManager->render();
	}
	return m_running;
}