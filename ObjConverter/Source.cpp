#include "Application.h"

int main(int argc, char *args[])
{
	Application* application = new Application();

	while (application->run())
		continue;

	delete application;

	return 0;
}