#pragma once

#include <iostream>
#include <memory>
#include <stack>
#include <iostream>
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Screen.h"
#include "ZRSFileLoader.h"
#include "OBJWriter.h"


class SimulationScreen : public Screen
{
public:
	/* Class Constructors & Destructors */
	SimulationScreen();
	virtual ~SimulationScreen();
	void renderSkybox();
	void renderWorld();

public:
	/* General Public Methods */
	virtual void init();
	virtual void update();
	virtual void render();
	

private:

	std::shared_ptr<ZRSFileLoader> zrsLoader;
	std::shared_ptr<OBJWriter> objWriter;

};