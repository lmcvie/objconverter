#include "OBJWriter.h"



OBJWriter::OBJWriter()
{
}


OBJWriter::~OBJWriter()
{
}


void OBJWriter::WriteToFile(std::string name, std::shared_ptr<ZRSFileLoader> zrsfileLoader)
{
	//create file in folder
	std::ofstream outfile(name);

	//place buffer data into file
	outfile << WriteOBJData(zrsfileLoader) << std::endl;

	//close file
	outfile.close();

}

std::string OBJWriter::WriteOBJData(std::shared_ptr<ZRSFileLoader> zrsfileLoader)
{
	//create stringstream buffer
	std::stringstream buffer;
	
	
	// small comment at top of obj file
	buffer << "#church and cockrel .obj converted from .zrf file" << std::endl;


//order for writing needs to be vertices -> texture verts -> texture normals -> faces to be in obj format. Then repeat for the 2nd mesh but adding an offset for the face indices
//x and z coords are switched places in new buffer as .zrf is left to right triangles but .obj is right to left triangles
#pragma region church mesh
	//write church vertices to buffer
	for (int i = 0; i < 210; ++i)
	{

		buffer << "v " << zrsfileLoader->getVerts()[i][2];
		buffer << " " << zrsfileLoader->getVerts()[i][1];
		buffer << " " << zrsfileLoader->getVerts()[i][0] << std::endl;
	}

	
	// write church txture verts to buffer
	for (int i = 0; i < 210; ++i) 
	{
		
		buffer << "vt " << zrsfileLoader->getVerts()[i][4];
		buffer << " " << zrsfileLoader->getVerts()[i][3] << std::endl;


	}
	
	// write church vertex normals to buffer
	for (int i = 0; i < 210; ++i)
	{

		buffer << "vn " << zrsfileLoader->getVerts()[i][7];
		buffer << " " << zrsfileLoader->getVerts()[i][6];
		buffer << " " << zrsfileLoader->getVerts()[i][5] << std::endl;
	}

	
	//write church faces to buffer (+1 as .obj starts at 1 whilst .zrf starts at 0
	for (int i = 0; i < 103; ++i) 
	{
		// change order for triangles so it matches the new vertice order
		buffer << "f " << ((zrsfileLoader->getIndices()[i][1])+1 )<< " ";
		buffer << ((zrsfileLoader->getIndices()[i][2]+1)) << " ";
		buffer << ((zrsfileLoader->getIndices()[i][0]+1))  << std::endl;
		
	}

#pragma endregion

#pragma region cockrel mesh
	//write cockrel vertices to buffer
	for (int i = 210; i < 234; ++i)
	{

		buffer << "v " << zrsfileLoader->getVerts()[i][2];
		buffer << " " << zrsfileLoader->getVerts()[i][1];
		buffer << " " << zrsfileLoader->getVerts()[i][0] << std::endl;
	}


	for (int i = 210; i < 234; ++i)
	{

		buffer << "vt " << zrsfileLoader->getVerts()[i][4];
		buffer << " " << zrsfileLoader->getVerts()[i][3] << std::endl;


	}

	// write church vertex normals to buffer
	for (int i = 210; i < 234; ++i)
	{

		buffer << "vn " << zrsfileLoader->getVerts()[i][7];
		buffer << " " << zrsfileLoader->getVerts()[i][6];
		buffer << " " << zrsfileLoader->getVerts()[i][5] << std::endl;
	}

	// create subgroup
	buffer << "g cockrel" << std::endl;

	// write cockrel faces to buffer

	//because it is merging 2 objects into 1 there needs to be a face offset + 210 as there is 210 vertices in the church object and +1 as .obj starts at f 1 and .vrs starts at f 0
	for (int i = 103; i < 125; ++i)
	{
		
		buffer << "f " << ((zrsfileLoader->getIndices()[i][1]) + 211) << " ";
		buffer << ((zrsfileLoader->getIndices()[i][2]) + 211) << " ";
		buffer << ((zrsfileLoader->getIndices()[i][0]) + 211) << std::endl;

	}
	
#pragma endregion
	return buffer.str(); // return the buffer to be written to file

}
